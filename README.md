# Short description
Home tasks for S-PRO Academy Django part

# Stack
Python 3.8 + Django 4.0.5

# Gitflow
master – base project branch  
django-week-*n* – master + home tasks *n* week
